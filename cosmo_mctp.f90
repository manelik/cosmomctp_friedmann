

program cosmo_mctp

! This program solves the Friedmann equations
! usinf a fourth order Runge-Kutta method

  implicit none

  real(8) :: alpha, alpha_p, alpha_aux  ! lapse function
  real(8) :: alpha_k1 , alpha_k2 , alpha_k3 , alpha_k4 ! RK coefficients

  real(8) :: a_scale, a_scale_p, a_scale_aux ! scale factor
  real(8) :: a_scale_k1, a_scale_k2, a_scale_k3, a_scale_k4 ! RK coefficients

  real(8) :: H, H_p, H_aux ! Expansion factor
  real(8) :: H_k1 , H_k2 , H_k3 , H_k4 ! RK coefficients

  real(8) :: rho, rho_p, rho_aux ! total density
  real(8) :: rho_k1 , rho_k2 , rho_k3 , rho_k4 ! RK coefficients

  real(8) :: rho_crit,irho_crit  ! Critical density and its inverse
  
  real(8) :: p, p_aux ! total pressure

  integer :: N_fluid  ! Number of fluid species
  real(8), allocatable, dimension(:) :: fluid_rho, fluid_rho_p, fluid_rho_aux, fluid_omega, fluid_p
  real(8), allocatable, dimension(:) :: fluid_rho_k1, fluid_rho_k2, fluid_rho_k3, fluid_rho_k4

  integer :: N_scalar ! Number of real scalar fields 

  real(8), allocatable, dimension(:) :: scalar_phi, scalar_phi_p, scalar_phi_aux, scalar_mass 
  real(8), allocatable, dimension(:) :: scalar_phi_k1,scalar_phi_k2,scalar_phi_k3,scalar_phi_k4   
  real(8), allocatable, dimension(:) :: scalar_pi, scalar_pi_p, scalar_pi_aux                     
  real(8), allocatable, dimension(:) :: scalar_pi_k1,scalar_pi_k2,scalar_pi_k3,scalar_pi_k4       


  integer :: k_curv    ! Curvature of space-like sections

  integer :: expansion ! Sign of expansion

!  real(8) :: fluid_omega

  character(30) :: lapse_type

  real(8) :: t, dt,dt0  !grid parameters
  integer :: Nt

  integer :: i, j, k !counters

  character(30) :: out_dir ! output directory
  

  real(8) :: smallpi, zero, one, two, three, half, third ! Numbers

! Numbers

  zero = 0.d0
  one  = 1.d0
  two  = 2.d0
  three= 3.d0
  half = 0.5d0
  third= one/three
  smallpi =  acos(-1.d0)



  write(*,*) "******************"
  write(*,*) "*                *"
  write(*,*) "*  COSMO-MCTP    *"
  write(*,*) "*                *"
  write(*,*) "******************"

!  write(*,*) "Lapso (propio, conforme)"
!  read(*,*) lapse_type

! Obtener parámetros

  write(*,*) "Factor de escala inicial"
  read(*,*) a_scale

  write(*,*) "Numero de especies de fluidos (polvo, radiacion, energia oscura)"
  read(*,*) N_fluid

! Una vez especificada la cantidad de fluidos asignamos espacio en memoria
  allocate(fluid_rho(1:N_fluid), fluid_rho_p(1:N_fluid), fluid_rho_aux(1:N_fluid))
  allocate(fluid_omega(1:N_fluid), fluid_p(1:N_fluid))
  allocate(fluid_rho_k1(1:N_fluid), fluid_rho_k2(1:N_fluid))
  allocate(fluid_rho_k3(1:N_fluid), fluid_rho_k4(1:N_fluid))

! Parametros de cada fluido
  do i = 1, N_fluid
     write(*,*) ""
     write(*,*) " Densidad inicial del fluido",i
     read(*,*) fluid_rho(i)

     write(*,*) " Constante omega del fluido", i 
     read(*,*) fluid_omega(i)

     fluid_p(i) = fluid_omega(i)*fluid_rho(i)
  end do

  write(*,*) "Numero de especies de campos escalares"
  read(*,*) N_scalar

! Una vez especificada la cantidad de campos escalares asignamos espacio en memoria
  allocate(scalar_phi(1:N_scalar), scalar_phi_p(1:N_scalar), scalar_phi_aux(1:N_scalar))
  allocate(scalar_mass(1:N_scalar) )
  allocate(scalar_phi_k1(1:N_scalar),scalar_phi_k2(1:N_scalar))
  allocate(scalar_phi_k3(1:N_scalar),scalar_phi_k4(1:N_scalar))
  allocate(scalar_pi(1:N_scalar), scalar_pi_p(1:N_scalar), scalar_pi_aux(1:N_scalar))
  allocate(scalar_pi_k1(1:N_scalar),scalar_pi_k2(1:N_scalar))
  allocate(scalar_pi_k3(1:N_scalar),scalar_pi_k4(1:N_scalar))

! Parametros de cada campo escalar
  do i = 1, N_scalar
     write(*,*) ""
     write(*,*) "Valor inicial del campo escalar", i
     read(*,*) scalar_phi(i)
     write(*,*) "Derivada inicial del campo escalar", i
     read(*,*) scalar_pi(i)

     write(*,*) "Masa del campo escalar ", i 
     read(*,*) scalar_mass(i)

  end do


!  write(*,*) "Densidad inicial"
!  read(*,*) rho

!  write(*,*) "omega (p = omega*rho)"
!  read(*,*) fluid_omega
  

  write(*,*) "Expansion o contraccion? (+1,-1)"
  read(*,*) expansion

  write(*,*) "Curvatura (k = 1,0,-1) "
  read(*,*) k_curv

  write(*,*) "Slicing?"
  read(*,*) lapse_type


  write(*,*) "Intervalo dt"
  read(*,*) dt0
  write(*,*) "Pasos Nt"
  read(*,*) Nt

  write(*,*) "directorio de salida"
  read(*,*) out_dir

! OUTPUT FILES

! Make directory
  call system('mkdir -p '//trim(out_dir))
! Open outfiles
  open(unit=88,file=trim(out_dir)//"/cosmo_mctp.dat",status="unknown")


! Initialize data

! time
  t = 0
  dt = dt0/a_scale

! Calculate total energy density and pressure

  rho = zero
  p = zero
  do i = 1, N_fluid
     rho = rho + fluid_rho(i)
     p   = p + fluid_p(i)
  end do

! Add scalar field densities
  do i = 1, N_scalar
     rho = rho + half*(scalar_pi(i)**2 + (scalar_mass(i)*scalar_phi(i))**2)
     p = p + half*(scalar_pi(i)**2 - (scalar_mass(i)*scalar_phi(i))**2)
  end do

! Solve hamiltonian for H  
  H = calculate_H(expansion,a_scale,rho,k_curv) 
  rho_crit = three*H**2/8.d0/smallpi
  irho_crit = 1.d0/rho_crit

! save initial values
  write(88,"(10ES16.8)") t, a_scale,H,rho, p, fluid_rho*irho_crit, &
       & half*(scalar_pi**2 + (scalar_mass*scalar_phi)**2)*irho_crit

! Alpha selection
  select case (lapse_type)
  case ("proper")
     alpha = 1.d0
  case ("conformal")
     alpha = a_scale
  case default
     print*, "Unknown slicing, using proper"
     alpha = 1.d0
     lapse_type = "proper"
  end select

! begin method

  do i = 1, Nt
! Save old time step

     a_scale_p = a_scale
     H_p = H

     fluid_rho_p = fluid_rho ! whole arrays

     scalar_phi_p = scalar_phi
     scalar_pi_p = scalar_pi 

     rho_p = rho ! La densidad total es la suma de todas las
                 ! densidades, por eso los coeficientes asociados a
                 ! ella son la suma de los coeficientes individuales

! Evaluate RK4 coefficients     

     ! First step on place
     H_aux = calculate_H(expansion,a_scale_p,&
          & rho_p,k_curv)

     alpha_aux = calculate_alpha(lapse_type,a_scale_p)

     !Geometry
     a_scale_k1 = S_a_scale(a_scale_p,H_aux,rho_p,alpha_aux) 
     rho_k1 = zero
     do j = 1, N_fluid  ! Fluids (radiation, dark energy)
        fluid_rho_k1(j) = S_rho(a_scale_p,H_aux,fluid_rho_p(j),alpha_aux, fluid_omega(j))
        rho_k1 = rho_k1 + fluid_rho_k1(j)
     end do
     do j = 1, N_scalar ! Scalar fields
        scalar_phi_k1(j)= S_scalar_phi(scalar_pi_p(j),alpha_aux)
        scalar_pi_k1(j) = S_scalar_pi(scalar_phi_p(j),scalar_pi_p(j),H_aux,alpha_aux, scalar_mass(j))
        rho_k1 = rho_k1 + S_scalar_rho(a_scale_p,H_aux,scalar_pi_p(j),alpha_aux) ! Calculamos aparte los ceficientes
     end do                                                                  ! de la densidad del campo


     ! Second step, half step forward
     H_aux = calculate_H(expansion,a_scale_p + half*dt*a_scale_k1,&
          & rho_p + half*dt*rho_k1,k_curv)
     alpha_aux = calculate_alpha(lapse_type,a_scale_p+half*dt*a_scale_k1)

     a_scale_k2 = S_a_scale(a_scale_p+half*dt*a_scale_k1,&
          & H_aux,rho_p+ half*dt*rho_k1,alpha_aux)
     rho_k2 = zero
     do j = 1, N_fluid
        fluid_rho_k2(j) = S_rho(a_scale_p+half*dt*a_scale_k1,H_aux,&
             & fluid_rho_p(j)+half*dt*fluid_rho_k1(j) ,alpha_aux, fluid_omega(j))
        rho_k2 = rho_k2 + fluid_rho_k2(j)
     end do
     do j = 1, N_scalar
        scalar_phi_k2(j) = S_scalar_phi(scalar_pi_p(j)+half*dt*scalar_pi_k1(j),alpha_aux)
        scalar_pi_k2(j) = S_scalar_pi(scalar_phi_p(j)+half*dt*scalar_phi_k1(j),&
             & scalar_pi_p(j)+half*dt*scalar_pi_k1(j),H_aux,alpha_aux, scalar_mass(j))
        rho_k2 = rho_k2 + S_scalar_rho(a_scale_p+half*dt*a_scale_k1,H_aux,&
             & scalar_pi_p(j)+half*dt*scalar_pi_k1(j),alpha_aux)
     end do

     ! Third step, half step forward
     H_aux = calculate_H(expansion,a_scale_p + half*dt*a_scale_k2,&
          & rho_p + half*dt*rho_k2,k_curv)
     alpha_aux = calculate_alpha(lapse_type,a_scale_p+half*dt*a_scale_k2)

     a_scale_k3 = S_a_scale(a_scale_p+half*dt*a_scale_k2,&
          & H_aux,rho_p+ half*dt*rho_k2,alpha_aux)
     rho_k3 = zero
     do j = 1, N_fluid
        fluid_rho_k3(j) = S_rho(a_scale_p+half*dt*a_scale_k2,H_aux,&
             & fluid_rho_p(j)+half*dt*fluid_rho_k2(j) ,alpha_aux, fluid_omega(j))
        rho_k3 = rho_k3 + fluid_rho_k3(j)
     end do
     do j = 1, N_scalar
        scalar_phi_k3(j) = S_scalar_phi(scalar_pi_p(j)+half*dt*scalar_pi_k2(j),alpha_aux)
        scalar_pi_k3(j) = S_scalar_pi(scalar_phi_p(j)+half*dt*scalar_phi_k2(j),&
             & scalar_pi_p(j)+half*dt*scalar_pi_k2(j),H_aux,alpha_aux, scalar_mass(j))
        rho_k3 = rho_k3 + S_scalar_rho(a_scale_p+half*dt*a_scale_k2,H_aux,&
             & scalar_pi_p(j)+half*dt*scalar_pi_k2(j),alpha_aux)
     end do

     ! Fourth step, full step forward
     H_aux = calculate_H(expansion,a_scale_p + dt*a_scale_k3,&
          & rho_p + dt*rho_k3,k_curv)
     alpha_aux = calculate_alpha(lapse_type,a_scale_p+dt*a_scale_k3)

     a_scale_k4 = S_a_scale(a_scale_p+dt*a_scale_k3,&
          & H_aux,rho_p+ dt*rho_k3,alpha_aux)
     rho_k4 = zero
     do j = 1, N_fluid
        fluid_rho_k4(j) = S_rho(a_scale_p+dt*a_scale_k3,H_aux,&
             & fluid_rho_p(j)+dt*fluid_rho_k3(j) ,alpha_aux, fluid_omega(j))
        rho_k4 = rho_k4 + fluid_rho_k4(j)
     end do
     do j = 1, N_scalar
        scalar_phi_k4(j) = S_scalar_phi(scalar_pi_p(j)+dt*scalar_pi_k3(j),alpha_aux)
        scalar_pi_k4(j) = S_scalar_pi(scalar_phi_p(j)+dt*scalar_phi_k3(j),&
             & scalar_pi_p(j)+dt*scalar_pi_k3(j),H_aux,alpha_aux, scalar_mass(j))
        rho_k4 = rho_k4 + S_scalar_rho(a_scale_p+dt*a_scale_k3,H_aux,&
             & scalar_pi_p(j)+dt*scalar_pi_k3(j),alpha_aux)
     end do
 
! Update value     
     a_scale = a_scale_p + dt/6.0*(a_scale_k1+2.0*a_scale_k2+2.0*a_scale_k3+a_scale_k4)

! Podemos operar directamente con los arreglos de fluidos y campos escalares
     fluid_rho = fluid_rho_p +&
          & dt/6.0*(fluid_rho_k1+2.0*fluid_rho_k2+2.0*fluid_rho_k3+fluid_rho_k4)

     scalar_phi = scalar_phi_p +&
          & dt/6.0*(scalar_phi_k1+2.0*scalar_phi_k2+2.0*scalar_phi_k3+scalar_phi_k4)
     scalar_pi = scalar_pi_p +&
          & dt/6.0*(scalar_pi_k1+2.0*scalar_pi_k2+2.0*scalar_pi_k3+scalar_pi_k4)

! Recalculamos la densidad y presion totales en el nuevo paso de tiempo
     rho = zero
     p = zero
     do j=1,N_fluid
        rho = rho + fluid_rho(j)
        fluid_p(j) = fluid_omega(j)*fluid_rho(j)
        p = p + fluid_p(j)
     end do
     do j = 1, N_scalar
        rho = rho + half*(scalar_pi(j)**2 + (scalar_mass(j)*scalar_phi(j))**2)
        p = p + half*(scalar_pi(j)**2 - (scalar_mass(j)*scalar_phi(j))**2)
     end do

! Calculamos la expansión al nuevo paso
     H = calculate_H(expansion,a_scale,rho,k_curv)
     rho_crit = three*H**2/8.d0/smallpi
     irho_crit = 1.d0/rho_crit
     alpha = calculate_alpha(lapse_type,a_scale)

! Advance t depending on the slicing

     select case (lapse_type)
     case ("proper")
        t = t+dt
     case ("conformal")
        t = t+dt
        dt= dt0/a_scale
     end select
! save to file
     write(88,"(10ES16.8)") t, a_scale,H,rho, p, fluid_rho*irho_crit,&
          & half*(scalar_pi**2 + (scalar_mass*scalar_phi)**2)*irho_crit

     
  end do

  close(88)

  write(*,*) "Finished! Have a nice day."

  contains

    function calculate_H(expansion,a_scale,rho,k_curv)
      implicit none

      real(8) calculate_H
      integer expansion,k_curv
      real(8) a_scale,rho

      real(8) smallpi
      smallpi =  acos(-1.d0)


      calculate_H = expansion*sqrt( 8.d0*smallpi/3.d0 * rho  - k_curv/a_scale**2 )
    end function calculate_H

    function S_a_scale(a_scale,H,rho,alphas)
      implicit none
      
      real(8) S_a_scale
      real(8) a_scale, H, rho, alphas 

      S_a_scale = a_scale*alphas*H
    end function S_a_scale

    function S_H(a_scale,H,rho,alphas,omega)
      implicit none

      real(8) S_H
      real(8) a_scale, H, rho, alphas, omega

      real(8) smallpi
      smallpi =  acos(-1.d0)

      S_H = -alphas*H**2-4.d0*smallpi/3.d0*alphas*rho*(1.d0+3.d0*omega)
    end function S_H

    function S_rho(a_scale,H,rho,alphas, omega)
      implicit none
      
      real(8) S_rho
      real(8) a_scale, H, rho, alphas, omega

      S_rho = -3.d0*alphas*H*rho*(1.d0+omega)
    end function S_rho

    function S_scalar_phi(pi,alphas)
      implicit none
      
      real(8) S_scalar_phi
      real(8) pi, alphas

      S_scalar_phi = alphas*pi
    end function S_scalar_phi
   
    function S_scalar_pi(phi,pi,H,alphas, mass)
      implicit none
      
      real(8) S_scalar_pi
      real(8) phi,pi, H, alphas, mass

      S_scalar_pi = -3.d0*alphas*H*pi - mass**2*alphas*phi
    end function S_scalar_pi

    function S_scalar_rho(a_scale,H,pi,alphas)
      implicit none
      
      real(8) S_scalar_rho
      real(8) a_scale, H, pi, rho, alphas

      S_scalar_rho = -3.d0*alphas*H*pi**2
    end function S_scalar_rho

    function calculate_alpha(lapse_type,a_scale)
      implicit none

      real(8) calculate_alpha
      character(30) lapse_type
      real(8)       a_scale

      select case (lapse_type)
      case ("proper")
         calculate_alpha = 1.d0
      case ("conformal")
         calculate_alpha = a_scale
      case default
         calculate_alpha = 1.d0
      end select

    end function calculate_alpha

end program COSMO_MCTP
